#[repr(u8)]
pub enum KeyCode {
    None = 0x00,
    KbErrRollOver = 0x01,
    KbPOSTFail = 0x02,
    KbErrUndef = 0x03,
    KbA = 0x04,
    KbB = 0x05,
    KbC = 0x06,
    KbD = 0x07,
    KbE = 0x08,
    KbF = 0x09,
    KbG = 0x0A,
    KbH = 0x0B,
    KbI = 0x0C,
    KbJ = 0x0D,
    KbK = 0x0E,
    KbL = 0x0F,
    KbM = 0x10,
    KbN = 0x11,
    KbO = 0x12,
    KbP = 0x13,
    KbQ = 0x14,
    KbR = 0x15,
    KbS = 0x16,
    KbT = 0x17,
    KbU = 0x18,
    KbV = 0x19,
    KbW = 0x1A,
    KbX = 0x1B,
    KbY = 0x1C,
    KbZ = 0x1D,
    Kb1 = 0x1E,
    Kb2 = 0x1F,
    Kb3 = 0x20,
    Kb4 = 0x21,
    Kb5 = 0x22,
    Kb6 = 0x23,
    Kb7 = 0x24,
    Kb8 = 0x25,
    Kb9 = 0x26,
    Kb0 = 0x27,
    KbEnter = 0x28,
    KbEscape = 0x29,
    KbBackspace = 0x2A,
    KbTab = 0x2B,
    KbSpace = 0x2C,
    KbHyphen = 0x2D,
    KbEq = 0x2E,
    KbLBrace = 0x2F,
    KbRBrace = 0x30,
    KbBackSlash = 0x31,
    KbIntHash = 0x32,
    KbSemi = 0x33,
    KbQuote = 0x34,
    KbGrave = 0x35,
    KbComma = 0x36,
    KbPeriod = 0x37,
    KbSlash = 0x38,
    KbCaps = 0x39,
    KbF1 = 0x3A,
    KbF2 = 0x3B,
    KbF3 = 0x3C,
    KbF4 = 0x3D,
    KbF5 = 0x3E,
    KbF6 = 0x3F,
    KbF7 = 0x40,
    KbF8 = 0x41,
    KbF9 = 0x42,
    KbF10 = 0x43,
    KbF11 = 0x44,
    KbF12 = 0x45,
    KbPrtScr = 0x46,
    KbScrLck = 0x47,
    KbPause = 0x48,
    KbIns = 0x49,
    KbHome = 0x4A,
    KbPageUp = 0x4B,
    KbDelete = 0x4C,
    KbEnd = 0x4D,
    KbPageDown = 0x4E,
    KbRightArr = 0x4F,
    KbLeftArr = 0x50,
    KbDownArr = 0x51,
    KbUpArr = 0x52,
    KpNumLck = 0x53,
    KpSlash = 0x54,
    KpStar = 0x55,
    KpHyphen = 0x56,
    KpPlus = 0x57,
    KpEnter = 0x58,
    Kp1 = 0x59,
    Kp2 = 0x5A,
    Kp3 = 0x5B,
    Kp4 = 0x5C,
    Kp5 = 0x5D,
    Kp6 = 0x5E,
    Kp7 = 0x5F,
    Kp8 = 0x60,
    Kp9 = 0x61,
    Kp0 = 0x62,
    KpPeriod = 0x63,
    KbIntBackslash = 0x64,
    KbApp = 0x65,
    KbPwr = 0x66,
    KpEq = 0x67,
    KbF13 = 0x68,
    KbF14 = 0x69,
    KbF15 = 0x6A,
    KbF16 = 0x6B,
    KbF17 = 0x6C,
    KbF18 = 0x6D,
    KbF19 = 0x6E,
    KbF20 = 0x6F,
    KbF21 = 0x70,
    KbF22 = 0x71,
    KbF23 = 0x72,
    KbF24 = 0x73,
    KbExec = 0x74,
    KbHelp = 0x75,
    KbMenu = 0x76,
    KbSelect = 0x77,
    KbStop = 0x78,
    KbAgain = 0x79,
    KbUndo = 0x7A,
    KbCut = 0x7B,
    KbCopy = 0x7C,
    KbPaste = 0x7D,
    KbFind = 0x7E,
    KbMute = 0x7F,
    KbVolUp = 0x80,
    KbVolDown = 0x81,
    KbLockingCaps = 0x82,
    KbLockingNumLck = 0x83,
    KbLockingScrLck = 0x84,
    KpComma = 0x85,
    KpEqSign = 0x86,
    KbInt1 = 0x87,
    KbInt2 = 0x88,
    KbInt3 = 0x89,
    KbInt4 = 0x8A,
    KbInt5 = 0x8B,
    KbInt6 = 0x8C,
    KbInt7 = 0x8D,
    KbInt8 = 0x8E,
    KbInt9 = 0x8F,
    KbLang1 = 0x90,
    KbLang2 = 0x91,
    KbLang3 = 0x92,
    KbLang4 = 0x93,
    KbLang5 = 0x94,
    KbLang6 = 0x95,
    KbLang7 = 0x96,
    KbLang8 = 0x97,
    KbLang9 = 0x98,
    KbAltErase = 0x99,
    KbSysReq = 0x9A,
    KbCancel = 0x9B,
    KbClear = 0x9C,
    KbPrior = 0x9D,
    KbReturn = 0x9E,
    KbSep = 0x9F,
    KbOut = 0xA0,
    KbOper = 0xA1,
    KbClearAgain = 0xA2,
    KbCrSel = 0xA3,
    KbExSel = 0xA4,
    ReservedA5 = 0xA5,
    ReservedA6 = 0xA6,
    ReservedA7 = 0xA7,
    ReservedA8 = 0xA8,
    ReservedA9 = 0xA9,
    ReservedAA = 0xAA,
    ReservedAB = 0xAB,
    ReservedAC = 0xAC,
    ReservedAD = 0xAD,
    ReservedAE = 0xAE,
    ReservedAF = 0xAF,
    Kp00 = 0xB0,
    Kp000 = 0xB1,
    ThousandsSep = 0xB2,
    DecimalSep = 0xB3,
    CurrencyUnit = 0xB4,
    CurrencySub = 0xB5,
    KpLParen = 0xB6,
    KpRParen = 0xB7,
    KpLBrace = 0xB8,
    KpRBrace = 0xB9,
    KpTab = 0xBA,
    KpBack = 0xBB,
    KpA = 0xBC,
    KpB = 0xBD,
    KpC = 0xBE,
    KpD = 0xBF,
    KpE = 0xC0,
    KpF = 0xC1,
    KpXOR = 0xC2,
    KpCarat = 0xC3,
    KpPercent = 0xC4,
    KpLt = 0xC5,
    KpGt = 0xC6,
    KpAmp = 0xC7,
    KpAmpAmp = 0xC8,
    KpPipe = 0xC9,
    KpPipePipe = 0xCA,
    KpColon = 0xCB,
    KpHash = 0xCC,
    KpSpace = 0xCD,
    KpAt = 0xCE,
    KpBang = 0xCF,
    KpMemStore = 0xD0,
    KpMemRecall = 0xD1,
    KpMemClear = 0xD2,
    KpMemAdd = 0xD3,
    KpMemSub = 0xD4,
    KpMemMul = 0xD5,
    KpMemDiv = 0xD6,
    KpPlusMinus = 0xD7,
    KpClear = 0xD8,
    KpClearEntry = 0xD9,
    KpBinary = 0xDA,
    KpOctal = 0xDB,
    KpDecimal = 0xDC,
    KpHex = 0xDD,
    ReservedDE = 0xDE,
    ReservedDF = 0xDF,
    KbLCtrl = 0xE0,
    KbLShift = 0xE1,
    KbLAlt = 0xE2,
    KbLGui = 0xE3,
    KbRCtrl = 0xE4,
    KbRShift = 0xE5,
    KbRAlt = 0xE6,
    KbRGui = 0xE7,
    ReservedE8 = 0xE8,
    ReservedE9 = 0xE9,
    ReservedEA = 0xEA,
    ReservedEB = 0xEB,
    ReservedEC = 0xEC,
    ReservedED = 0xED,
    ReservedEE = 0xEE,
    ReservedEF = 0xEF,
    ReservedF0 = 0xF0,
    ReservedF1 = 0xF1,
    ReservedF2 = 0xF2,
    ReservedF3 = 0xF3,
    ReservedF4 = 0xF4,
    ReservedF5 = 0xF5,
    ReservedF6 = 0xF6,
    ReservedF7 = 0xF7,
    ReservedF8 = 0xF8,
    ReservedF9 = 0xF9,
    ReservedFA = 0xFA,
    ReservedFB = 0xFB,
    ReservedFC = 0xFC,
    ReservedFD = 0xFD,
    ReservedFE = 0xFE,
    ReservedFF = 0xFF,
}
