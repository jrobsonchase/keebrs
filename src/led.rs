/// Trait for LED output.
///
/// Default implementation does nothing. Override the provided methods for your
/// specific hardware.
pub trait SetLED {
    #[inline]
    fn set_caps(&self, _state: bool) {}

    #[inline]
    fn set_num(&self, _state: bool) {}

    #[inline]
    fn set_scroll(&self, _state: bool) {}

    #[inline]
    fn set_compose(&self, _state: bool) {}

    #[inline]
    fn set_kana(&self, _state: bool) {}
}
