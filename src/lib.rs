#![feature(nll)]
#![no_std]
#![cfg_attr(not(feature = "static_alloc"), feature(alloc))]

#[cfg(not(feature = "static_alloc"))]
#[macro_use]
extern crate alloc;

pub mod keycode;
pub mod led;
pub mod scan;
pub mod state;

pub(crate) mod prelude {
    #[cfg(not(feature = "static_alloc"))]
    pub use crate::alloc::prelude::*;

    pub use crate::{
        keycode::*,
        led::*,
        scan::*,
        state::*,
    };

    pub use core::ops::{
        Deref,
        DerefMut,
    };
}
