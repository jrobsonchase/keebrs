#[cfg(feature = "static_alloc")]
use crate::prelude::*;

/// Trait for a keyboard matrix scanning
pub trait Scan {
    const ROWS: usize;
    const COLS: usize;

    /// Container capable of holding the state for this `Scan`ner
    ///
    /// This won't be needed once const generics come to be. Until then, it
    /// should always be `[[bool; COLS]; ROWS]`.
    #[cfg(feature = "static_alloc")]
    type State: State + Default;

    /// Drive or release a row
    fn drive_row(&mut self, row: usize, state: bool);
    /// Test a column
    fn read_col(&self, col: usize) -> bool;

    /// Convenience method for automatic row driving/releasing
    fn with_row<F, R>(&mut self, row: usize, f: F) -> R
    where
        F: FnOnce(&mut Self) -> R,
    {
        self.drive_row(row, true);
        let res = f(self);
        self.drive_row(row, false);
        res
    }
}
