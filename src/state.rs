use crate::prelude::*;

pub trait State {
    fn get_state(&self, row: usize, col: usize) -> bool;
    fn set_state(&mut self, row: usize, col: usize, state: bool);
}

#[cfg(not(feature = "static_alloc"))]
impl State for Vec<Vec<bool>> {
    #[inline]
    fn get_state(&self, row: usize, col: usize) -> bool {
        self[row][col]
    }
    #[inline]
    fn set_state(&mut self, row: usize, col: usize, state: bool) {
        self[row][col] = state;
    }
}

// When const generics come to be, this should be used instead of the macro
// below it
// impl<const ROWS: usize, const COLS: usize> State for [[bool; COLS]; ROWS] {
//     #[inline]
//     fn get_state(&self, row: usize, col: usize) -> bool {
//         self[row][col]
//     }
//     #[inline]
//     fn set_state(&mut self, row: usize, col: usize, state: bool) {
//         self[row][col] = state;
//     }
// }

#[cfg(feature = "static_alloc")]
macro_rules! impl_state {
    (@col $row:expr ; [$($col:expr),*]) => {
        $(
            impl State for [[bool; $col]; $row] {
                #[inline]
                fn get_state(&self, row: usize, col: usize) -> bool {
                    self[row][col]
                }
                #[inline]
                fn set_state(&mut self, row: usize, col: usize, state: bool) {
                    self[row][col] = state;
                }
            }
        )*
    };
    (@both [$($row:expr),*] ; $cols:tt) => {
        $(
            impl_state!(@col $row; $cols);
        )*
    };
    ($($n:expr),*) => {
        impl_state!(@both [$($n),*]; [$($n),*]);
    };
}

#[cfg(feature = "static_alloc")]
impl_state!(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);

/// Trait for scanning with state
///
/// Rather than row/column level manipulation, this will return only the
/// press/release events.
///
/// Note that it assumes that there will be only one change per scan.
pub trait StatefulScan {
    fn get_change(&mut self) -> Option<((usize, usize), bool)>;
}

pub struct StatefulScanner<S>
where
    S: Scan,
{
    // For when const generics happen
    // state: [[bool; <S as Scan>::COLS]; <S as Scan>::ROWS],
    #[cfg(not(feature = "static_alloc"))]
    state: Vec<Vec<bool>>,
    #[cfg(feature = "static_alloc")]
    state: S::State,
    scanner: S,
}

impl<S> Deref for StatefulScanner<S>
where
    S: Scan,
{
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &self.scanner
    }
}

impl<S> DerefMut for StatefulScanner<S>
where
    S: Scan,
{
    fn deref_mut(&mut self) -> &mut S {
        &mut self.scanner
    }
}

impl<S> StatefulScanner<S>
where
    S: Scan,
{
    pub fn new(scanner: S) -> Self {
        StatefulScanner {
            #[cfg(feature = "static_alloc")]
            state: S::State::default(),
            #[cfg(not(feature = "static_alloc"))]
            state: vec![vec![false; S::COLS]; S::ROWS],
            scanner,
        }
    }
}

impl<S> StatefulScan for StatefulScanner<S>
where
    S: Scan,
{
    fn get_change(&mut self) -> Option<((usize, usize), bool)> {
        let scanner = &mut self.scanner;
        let state = &mut self.state;
        for row in 0..S::ROWS {
            let state = scanner.with_row(row, |scanner| {
                for col in 0..S::COLS {
                    let current = scanner.read_col(col);
                    let previous = state.get_state(row, col);
                    if previous != current {
                        state.set_state(row, col, current);
                        return Some(((row, col), current));
                    }
                }
                None
            });

            if state.is_some() {
                return state;
            }
        }
        None
    }
}
